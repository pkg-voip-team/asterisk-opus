Source: asterisk-opus
Section: comm
Priority: optional
Maintainer: Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Build-Depends: cdbs,
 debhelper,
 dh-buildinfo,
 asterisk-dev,
 libopus-dev,
 libopusfile-dev
Standards-Version: 4.2.1
Vcs-Git: https://salsa.debian.org/pkg-voip-team/asterisk-opus.git
Vcs-Browser: https://salsa.debian.org/pkg-voip-team/asterisk-opus
Homepage: https://github.com/traud/asterisk-opus/
Rules-Requires-Root: no

Package: asterisk-opus
Architecture: any
Depends: ${cdbs:Depends},
 ${misc:Depends},
 ${shlibs:Depends}
Description: opus module for Asterisk
 Module for the Asterisk open source PBX which allows you to use the
 Opus audio codec.
 .
 Opus is the default audio codec in WebRTC. WebRTC is available in
 Asterisk via SIP over WebSockets (WSS). Nevertheless, Opus can be used
 for other transports (UDP, TCP, TLS) as well. Opus supersedes previous
 codecs like CELT and SiLK. Furthermore in favor of Opus, other
 open-source audio codecs are no longer developed, like Speex, iSAC,
 iLBC, and Siren. If you use your Asterisk as a back-to-back user agent
 (B2BUA) and you transcode between various audio codecs, one should
 enable Opus for future compatibility.
 .
 Opus is not only supported for pass-through but can be transcoded as
 well. This allows you to translate to/from other audio codecs like
 those for landline telephones (ISDN: G.711; DECT: G.726-32; and HD:
 G.722) or mobile phones (GSM, AMR, AMR-WB, 3GPP EVS).
