#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2016-2017 Jonas Smedegaard <dr@jones.dk>
# Description: Main Debian packaging script for asterisk-opus
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

include /usr/share/cdbs/1/rules/upstream-tarball.mk
include /usr/share/cdbs/1/class/makefile.mk
include /usr/share/cdbs/1/rules/debhelper.mk

pkg = $(DEB_SOURCE_PACKAGE)

DEB_UPSTREAM_URL = https://github.com/traud/asterisk-opus/archive

# needed (always) at runtime
deps = asterisk

# Depend on virtual asterisk API package (if resolved)
ASTERISK_ABI := $(shell grep -Po 'AST_BUILDOPT_SUM "\K[^"]+' /usr/include/asterisk/buildopts.h)
$(if $(ASTERISK_ABI),,$(call cdbs_warn,failed to resolve AST_BUILDOPT_SUM))
deps += $(ASTERISK_ABI:%=$(comma) asterisk-%)

CDBS_DEPENDS_$(pkg) = $(deps)

DEB_MAKE_INSTALL_TARGET = install DESTDIR=$(DEB_DESTDIR) ASTMODDIR=/usr/lib/asterisk/modules

CFLAGS += -fPIC

# avoid res_format module when shipped with Asterisk
binary-post-install/$(pkg)::
	$(if $(wildcard /usr/include/asterisk/opus.h),rm -f $(cdbs_curdestdir)/usr/lib/asterisk/modules/res_format_attr_opus.so)
